package com.example.sala304b.cachoeira;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RatingBar;
import android.widget.TextView;

public class DetalheActivity extends AppCompatActivity {

    private Cachoeira cachoeira;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent= getIntent();

        cachoeira = (Cachoeira) intent.getSerializableExtra(MainActivity.CACHOEIRA);

        if (cachoeira != null){
            TextView textViewNome = findViewById(R.id.nome);
            TextView textViewInformacoes = findViewById(R.id.informacoes);
            RatingBar ratingBarClassificacao = findViewById(R.id.rtClassificacao);

            textViewNome.setText(cachoeira.getNome());
            textViewInformacoes.setText(cachoeira.getInformocoes());
            ratingBarClassificacao.setRating(cachoeira.getClassificacao());
        }
    }
}
